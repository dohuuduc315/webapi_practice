﻿using CRUDWithEFCore.IRepositories;
using CRUDWithEFCore.Repositories;
using Microsoft.EntityFrameworkCore;

namespace CRUDWithEFCore
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDbContext _context;
        private readonly IProductRepository _productRepository;

        public UnitOfWork(AppDbContext context)
        {
            _context = context;
            _productRepository = new ProductRepository(_context);
        }
        public IProductRepository ProductRepository => _productRepository;

        public void Dispose()
        {
            _context.Dispose();
        }

        public async Task<int> SaveChangeAsync() => await _context.SaveChangesAsync();
    }
}
