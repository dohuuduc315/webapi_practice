﻿using CRUDWithEFCore.Entities;

namespace CRUDWithEFCore.IRepositories
{
    public interface IGenericRepository<T> where T : BaseEntity
    {
        Task AddAsync(T entity);
        void Update(T entity);
        void SoftRemove(T entity);
        Task<List<T>> GetAllAsync();
        Task<T?> GetByIdAsync(int id);
    }
}
