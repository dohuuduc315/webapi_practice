﻿using CRUDWithEFCore.Entities;

namespace CRUDWithEFCore.IRepositories
{
    public interface IProductRepository : IGenericRepository<Product>
    {
    }
}
