﻿using CRUDWithEFCore.IRepositories;

namespace CRUDWithEFCore
{
    public interface IUnitOfWork : IDisposable
    {
        public IProductRepository ProductRepository { get; }
        public Task<int> SaveChangeAsync();
    }
}
