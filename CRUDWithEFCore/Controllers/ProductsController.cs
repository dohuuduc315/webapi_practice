﻿using CRUDWithEFCore.Entities;
using CRUDWithEFCore.Interfaces;
using CRUDWithEFCore.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CRUDWithEFCore.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly IProductService _productService;

        public ProductsController(IProductService productService)
        {
            _productService = productService;
        }

        [HttpGet]
        public async Task<List<Product>> GetAllProducts() => await _productService.GetAllProducts();

        [HttpPost]
        public async Task CreateProduct(Product product) => await _productService.AddProduct(product);

        [HttpDelete("{id}")]
        public async Task RemoveProduct(int id) => await _productService.DeleteProduct(id);

        [HttpPut("{id}")]
        public async Task UpdateProduct(int id) => await _productService.UpdateProduct(id);
    }
}
