using CRUDWithEFCore;
using CRUDWithEFCore.Interfaces;
using CRUDWithEFCore.Services;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllers();
builder.Services.AddScoped<IProductService, ProductService>();
builder.Services.AddScoped<IUnitOfWork, UnitOfWork>();

var conn = "Data Source=DESKTOP-Q5ODVD3\\SQLEXPRESS;user=sa;password=12345;Database=CRUDWithEFCore;Trusted_Connection=True;TrustServerCertificate=True";
// todo register with dboption
builder.Services.AddDbContext<AppDbContext>(x => x.UseSqlServer(conn));

var app = builder.Build();

// Configure the HTTP request pipeline.

app.MapControllers();

app.Run();
