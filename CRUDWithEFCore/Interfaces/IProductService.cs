﻿using CRUDWithEFCore.Entities;

namespace CRUDWithEFCore.Interfaces
{
    public interface IProductService
    {
        Task AddProduct(Product product);
        Task UpdateProduct(int id);
        Task DeleteProduct(int id);
        Task<List<Product>> GetAllProducts();
    }
}
