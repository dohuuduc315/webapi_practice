﻿using CRUDWithEFCore.Entities;
using CRUDWithEFCore.Interfaces;
using Microsoft.IdentityModel.Tokens;

namespace CRUDWithEFCore.Services
{
    public class ProductService : IProductService
    {
        private readonly IUnitOfWork _unitOfWork;

        public ProductService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public async Task AddProduct(Product product)
        {
            await _unitOfWork.ProductRepository.AddAsync(product);
            await _unitOfWork.SaveChangeAsync();
        }

        public async Task DeleteProduct(int id)
        {
            var productObj = await _unitOfWork.ProductRepository.GetByIdAsync(id);
            if (productObj is not null)
            {
                _unitOfWork.ProductRepository.SoftRemove(productObj);
                await _unitOfWork.SaveChangeAsync();
            }
        }

        public async Task<List<Product>> GetAllProducts() => await _unitOfWork.ProductRepository.GetAllAsync();

        public async Task UpdateProduct(int id)
        {
            var productObj = await _unitOfWork.ProductRepository.GetByIdAsync(id);
            if (productObj is not null)
            {
                _unitOfWork.ProductRepository.Update(productObj);
                await _unitOfWork.SaveChangeAsync();
            }
            
        }
    }
}
