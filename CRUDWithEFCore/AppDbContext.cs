﻿using CRUDWithEFCore.Entities;
using Microsoft.EntityFrameworkCore;

namespace CRUDWithEFCore
{
    public class AppDbContext : DbContext
    {
        public DbSet<Product> Products { get; set; }
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }
    }
}
