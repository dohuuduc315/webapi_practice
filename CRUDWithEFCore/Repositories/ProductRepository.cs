﻿using CRUDWithEFCore.Entities;
using CRUDWithEFCore.IRepositories;

namespace CRUDWithEFCore.Repositories
{
    public class ProductRepository : GenericRepository<Product>, IProductRepository
    {
        public ProductRepository(AppDbContext context) : base(context) { }
    }
}
