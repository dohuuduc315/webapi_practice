﻿using CRUDWithEFCore.Entities;
using CRUDWithEFCore.IRepositories;
using Microsoft.EntityFrameworkCore;

namespace CRUDWithEFCore.Repositories
{
    public class GenericRepository<T> : IGenericRepository<T> where T : BaseEntity
    {
        private readonly AppDbContext _context;
        protected DbSet<T> _dbSet;

        public GenericRepository(AppDbContext context)
        {
            _context = context;
            _dbSet = _context.Set<T>();

        }
        public async Task AddAsync(T entity) => await _dbSet.AddAsync(entity);

        public Task<List<T>> GetAllAsync() => _dbSet.ToListAsync();

        public async Task<T?> GetByIdAsync(int id) => await _dbSet.FirstOrDefaultAsync(x => x.Id == id);

        public void SoftRemove(T entity)
        {
            entity.isDeleted = true;
            _dbSet.Update(entity);
        }

        public void Update(T entity) => _dbSet.Update(entity);
    }
}
